/*
// ==UserScript==
// @name    Gmail Date Isofy
// @version 1
// @grant   none
// @match   https://mail.google.com/*
// ==/UserScript==
*/

/* Note to self: DO NOT EDIT THIS in GreaseMonkey; original source found in my
 * bookmarklets folder; edit there and copy here */

/* 
   ==Error debugging==
   
   Exceptions caught by GreaseMonkey won't show up in a tab's console; need to
   check the browser console.  First, open a tab's console, and then press
   ctrl-shift-j to pop open the browser console. 
*/

javascript:{
  const de2en_weekday = {
    "Mo": "Monday",
    "Di": "Tuesday",
    "Mi": "Wednesday",
    "Do": "Thursday",
    "Fr": "Friday",
    "Sa": "Saturday",
    "So": "Sunday"
  };
  const de_weekdays_re = "(" + Object.keys (de2en_weekday).join ("|") + ")";

  const de2en_month = {
    "Jan.": "January",
    "Feb.": "February",
    "März": "March",
    "Apr.": "April",
    "Mai": "May",
    "Juni": "June",
    "Juli": "July",
    "Aug.": "August",
    "Sept.": "September",
    "Okt.": "October",
    "Nov.": "November",
    "Dez.": "December"
  };
  const de_months_re = "(" + Object.keys (de2en_month).join ("|") + ")";
  const de_date_re = "^" + de_weekdays_re + "., [0-9]+\. " + de_months_re + " (19|20)[0-9]{2}, [0-9]+:[0-9]{2}$";

  const en_weekday_short = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];

  /* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString */
  function gdi_pad (number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }
  
  function gdi_to_friendly_iso (date) {
    return en_weekday_short [date.getDay ()] + 
      ' ' + date.getFullYear() +
      '-' + gdi_pad (date.getMonth() + 1) +
      '-' + gdi_pad (date.getDate()) +
      ' ' + gdi_pad (date.getHours()) +
      ':' + gdi_pad (date.getMinutes());
  }

  function gdi_isofy_dates () {
    let date_tds = document.querySelectorAll (".xW:not(.date_isofied)");

    for (let i = 0; i < date_tds.length; i++) {
      let date_td = date_tds[i];

      /* grab the date span and the full text date from its title attribute */
      let date_span = date_td.firstElementChild;
      if (!date_span) {
        console.log ("Error: date_tds["+i+"] (.xW) does not have child elements, likely no date.");
        continue;
      }
      if (date_span.tagName != "SPAN") {
	console.log ("Error: date_tds["+i+"]'s first element child is not a SPAN element:" + date_td.innerHTML)
        continue;
      }

      let full_date_str = date_span.title;

      /* detect German date and translate to English */
      let de_match = full_date_str.match (de_date_re);
      if (de_match) {
        let de_weekday = de_match[1];
        let de_month = de_match[2];

        full_date_str = full_date_str.replace (de_weekday + "\.", de2en_weekday[de_weekday]).replace ("\. " + de_month, " " + de2en_month[de_month]);
      }

      /* convert date string to a "friendly" ISO format, {WeekDay} Y-M-D H:M */
      let iso_date_str = gdi_to_friendly_iso (new Date (full_date_str));

      let date_span_2 = date_span.firstElementChild;
      if (!date_span_2 || date_span_2.tagName != "SPAN") {
        console.log ("Error: date_tds["+i+"]'s date SPAN is missing its inner date span? " + date_td.innerHTML);
        continue;
      }
      date_span_2.innerHTML = iso_date_str;

      date_td.className += " date_isofied";
    }
  }

  function gdi_main_loop () {
    window.addEventListener ("load", function () { 
      setInterval (function () {
        gdi_isofy_dates ();
      }, 5000);    
    }, false);
  }
  
  console.log ("gdi: GMail Date Isofiy loaded");
  gdi_main_loop ();
  void (0);
}
