# Gmail Date Isofy

Converts the dates in the message list from natural formats into '{WeekDay} Y-m-d H:M'

## License

GPLv3

## Usage

This can either be used as a GreaseMonkey script or as a bookmarklet.

## Help

aquarichy (at) gmail (dot) com